﻿namespace Splorr.Seafarers.Models

type Dms =
    {
        Degrees: int
        Minutes: int
        Seconds: float
    }