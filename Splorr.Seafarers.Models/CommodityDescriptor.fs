﻿namespace Splorr.Seafarers.Models

type CommodityDescriptor =
    {
        Name: string
        BasePrice: float
        PurchaseFactor: float
        SaleFactor: float
        Discount: float
        Occurrence: float
    }

